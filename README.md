# Avris Dojo Playground

See: [dojo.avris.it](https://dojo.avris.it)

[paste here the instructions to whatever exercise you're doing]

## Copyright

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
