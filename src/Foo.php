<?php

namespace App;

class Foo
{
    public function bar(int $value): int
    {
        return $value + 1;
    }
}
